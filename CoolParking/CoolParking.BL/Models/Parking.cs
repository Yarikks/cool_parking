﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    class Parking
    {
        private static Parking _parking;
        public static List<Vehicle> listVehicles = new List<Vehicle>();
        public ReadOnlyCollection<Vehicle> vehicles = new ReadOnlyCollection<Vehicle>(listVehicles);
        public decimal parkBalance { get; set; }

        private Parking() { }

        public static Parking GetParking()
        {
            if (_parking == null)
            {
                _parking = new Parking();
            }
            return _parking;
        }
    }
}