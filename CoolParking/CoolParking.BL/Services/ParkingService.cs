﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        TimerService withdrawTimer = new TimerService(5000);
        TimerService logTimer = new TimerService(60000);
        LogService logService = new LogService(@"C:\Users\Yorik\Desktop\Cool_Racing\bsa20-dotnet-hw2-template\CoolParking\Transaction.log");

        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {

        }
        public ParkingService() { }

        private List<Vehicle> VehiclesInPark()
        {
            return Parking.listVehicles;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            var existVehicle = Parking.listVehicles.Find(v => v.Id == vehicle.Id);

            if (existVehicle == null)
            {
                if (VehiclesInPark().Count == GetCapacity())
                {
                    throw new InvalidOperationException();
                }
                else
                {
                    VehiclesInPark().Add(vehicle);
                }
            }
            else if (vehicle.Id == existVehicle.Id)
            {
                throw new ArgumentException();
            }
        }

        public void Dispose()
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();

            GC.SuppressFinalize(this);
        }

        public decimal GetBalance()
        {
            return Parking.GetParking().parkBalance;
        }

        public int GetCapacity()
        {
            return 10;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - Parking.GetParking().vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        { 
            return Settings.transactions.ToArray<TransactionInfo>();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.GetParking().vehicles;
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = VehiclesInPark().Find(veh => veh.Id == vehicleId);

            if (VehiclesInPark().Contains(vehicle))
            {
                VehiclesInPark().Remove(vehicle);
            }
            else
            {
                throw new ArgumentException("Remove unexisting vehicle.");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = VehiclesInPark().Find(veh => veh.Id == vehicleId);

            if (sum <= 0)
            {
                throw new ArgumentException();
            }
            else if (vehicle == null)
            {
                throw new ArgumentException();
            }
            else
            {
                vehicle.Balance += sum;
            }
        }
    }
}