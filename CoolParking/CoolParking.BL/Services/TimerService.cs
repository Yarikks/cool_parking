﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }
        public event ElapsedEventHandler Elapsed;
        private Timer timer;
        private LogService logService = new LogService("./Transaction.log");
        public TimerService(double interval)
        {
            Interval = interval;
            timer = new Timer(interval);
            timer.Elapsed += FireElapsedEvent;
        }

        public void FireElapsedEvent(object sender, ElapsedEventArgs e)
        {
            Start();
        }
        public void Dispose()
        {
            
        }

        public void Start()
        {
            TransactionInfo info = new TransactionInfo();

            string logInfo = "";
            foreach (Vehicle v in Parking.listVehicles)
            {
                v.Balance -= Settings.GetTariff(v.VehicleType);
                Parking.GetParking().parkBalance += Settings.GetTariff(v.VehicleType);
                logInfo += $"{DateTime.Now.ToLongTimeString()}: {v.VehicleType} payed {Settings.GetTariff(v.VehicleType)} to park service.";
                info.TransTime = DateTime.Now;
                info.VehicleId = v.Id;
                info.Sum += Settings.GetTariff(v.VehicleType);
            }
            Settings.transactions.Add(info);

            logService.Write(logInfo);
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}